# Coding Challenge

Welcome to a little Swisscom Health coding challenge. The aim of this challange is to get a brief overview about your skills and the way you work.

Don't worry, it will be just a small challenge we're asking you to do. You will create a simple rest API which gets data from an other API (SOAP) and then returns a JSON response.

## What to deliver

A functioning rest API implemented with Spring Boot which is calling a specific SOAP API to get a patient data record back.

- Rest API implemented with Spring Boot
- Dockerize the application
- Jenkinsfile which builds a deployable docker image
- Readme which contains all necessary and useful information about the project


## Minimal functional spec

The API should provide an endpoint to get demografic data of a patient by providing a country code.

Your API should get the data from a SOAP API provided by `gazelle` an IHE (International Health Exchange) standard framework.

- **Webservice definition** https://gazelle.ihe.net/gazelle-documentation/Demographic-Data-Server/user.html#web-service-definition
- **WSDL** https://gazelle.ihe.net/DemographicDataServer-ejb/DemographicDataServerService/DemographicDataServer?wsdl

You should use the `returnSimplePatient` method which returns a simple patient from a specific country.


## Minimal technical spec

- The Rest API should be implemented in Spring Boot
- The Rest API should contain one endpoint to get a patient as a JSON response by providing a country code
- There should be implemented a Jenkins file which builds a deployable docker image from this API

## How we review

Your application will be reviewed by at least two of our engineers. We do take into consideration your experience level.

**We value quality over feature-completeness**. It is fine to leave things aside provided you call them out in your project's README. The goal of this code sample is to help us identify what you consider production-ready code. You should consider this code ready for final review with your colleague, i.e. this would be the last step before deploying to production.

The aspects of your code we will assess include:

- **Architecture**: how clean is the separation between the front-end and the back-end?
- **Documentation and tracebility**: is the application, the API, the buildprocess documented with suitable tools/formats? Are technical trade-offs explained. 
- **Correctness**: does the application do what was asked? If there is anything missing, does the README explain why it is missing?
- **Code quality**: is the code simple, easy to understand, and maintainable? Are there any code smells or other red flags? Does object-oriented code follow principles such as the single responsibility principle? Is the coding style consistent with the language's guidelines? Is it consistent throughout the codebase?
- **Security**: are there any obvious vulnerabilities?
- **Testing**: how thorough are the automated tests? Will they be difficult to change if the requirements of the application were to change? Are there some unit and some integration tests? We're not looking for full coverage (given time constraint) but just trying to get a feel for your testing skills.
- **Automation**: is everything automated that makes sense?
- **Container**: is the app container small and secure
- **Technical choices**: do choices of libraries, databases, architecture etc. seem appropriate for the chosen application?
- **Production-readiness**: does the code include monitoring? logging? proper error handling?


## How to deliver

You can upload your code to a repository of your choice and provide us the link to it
